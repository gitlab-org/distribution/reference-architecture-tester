# Reference Architecture Tester

Spin up reference architecture deployments using GET and run QA against it.
Currently supports 1k, 2k, and 3d architectures, as well as a specialized one
(implementing PG HA using Patroni) for testing `omnibus-gitlab` packages.

By default, uses 1k architecture.

**`NOTE`**: Only maintainers of this project can currently run pipelines against
master branch. Please ask in `#g_distribution` Slack channel if you need help
with triggering a pipeline from your `omnibus-gitlab` pipeline.

If you are opening an MR in this project, a manual pipeline will be available
for you to test your changes.

#### Environment variables

| Variable                 | Details                                                                                                |
| :---                     | :---                                                                                                   |
| `NIGHTLY`                | Use nightly repo instead of release repo to download packages from. Specify `true` or `false`.         |
| `PACKAGE_URL`            | Specify URL to a publicly available package to be deployed.                                            |
| `PACKAGE_VERSION`        | Specify a specific package version to install from the repos. Example: `gitlab-ee=13.12.0+ee.0`        |
| `REFERENCE_ARCHITECTURE` | Specify which architecture to deploy. Valid values are names of directories inside `config/terraform`. |
| `QA_IMAGE`               | Specify which QA image to be used to run QA tests. Should ideally match the package being installed.   |

#### Local usage

1. Get JSON file corresponding to service account, private key of the SSH key pair with access to that service account, and EE license.

1. Clone this project and change directory to it

   ```
   git clone https://gitlab.com/gitlab-org/distribution/reference-architecture-tester.git
   cd reference-architecture-tester
   ```

1. Edit [config/.env](https://gitlab.com/gitlab-org/distribution/reference-architecture-tester/-/blob/master/config/.env) file and fill in the details. Source that file to get all the necessary environment variables

   ```
   source config/.env
   ```

1. Clone the GET project

   ```
   git clone https://gitlab.com/gitlab-org/quality/gitlab-environment-toolkit.git
   ```

1. Populate the credentials and other configuration details for terraform

   ```
   bash scripts/prepare_environment.sh terraform
   ```

1. Provision the infrastructure using terraform and get the external IP

   ```
   cd gitlab-environment-toolkit/terraform/environments
   terraform init
   terraform apply -auto-approve
   export EXTERNAL_IP=$(terraform output -json gitlab_ref_arch_gcp | jq -r '.haproxy_external.external_addresses[0]')
   cd ../../..
   ```

1. Populate the credentials and other configuration details for ansible

   ```
   bash scripts/prepare_environment.sh ansible
   ```

1. Configure the deployment using ansible

   ```
   cd gitlab-environment-toolkit/ansible
   pip3 install -r requirements/ansible-python-packages.txt
   ansible-galaxy install -r requirements/ansible-galaxy-requirements.yml

   ansible-playbook -i inventories/rat all.yml
   cd ../..
   ```

1. Run QA

   ```
   SIGNUP_DISABLED=true GITLAB_USERNAME=root GITLAB_PASSWORD=${GITLAB_INITIAL_PASSWORD} GITLAB_ADMIN_USERNAME=root GITLAB_ADMIN_PASSWORD=${GITLAB_INITIAL_PASSWORD} gitlab-qa Test::Instance::Any ${QA_IMAGE} http://$EXTERNAL_IP
   ```
