module "gitlab_ref_arch_gcp" {
  source = "../modules/gitlab_ref_arch_gcp"

  prefix = var.prefix
  project = var.project
  service_account_prefix = var.sa_prefix

  # Required for CloudSQL to avoid VPC clashes with multiple builds
  create_network = true

  machine_image = "rhel-8"

  # SCRAM-SHA-256 Postgres for FIPS
  cloud_sql_postgres_machine_tier = "custom-1-3840"
  cloud_sql_postgres_disk_size    = 50

  gitlab_rails_node_count = 1
  gitlab_rails_machine_type = "n1-standard-8"

  # GET must-haves
  haproxy_external_node_count = 1
  haproxy_external_machine_type = "g1-small"
}

output "gitlab_ref_arch_gcp" {
  value = module.gitlab_ref_arch_gcp
}
