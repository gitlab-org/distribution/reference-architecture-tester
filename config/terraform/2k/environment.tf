module "gitlab_ref_arch_gcp" {
  source = "../modules/gitlab_ref_arch_gcp"

  prefix = var.prefix
  project = var.project
  service_account_prefix = var.sa_prefix

  machine_image = "ubuntu-2204-lts"

  gitaly_node_count = 1
  gitaly_machine_type = "n1-standard-4"

  gitlab_rails_node_count = 2
  gitlab_rails_machine_type = "n1-standard-8"

  haproxy_external_node_count = 1
  haproxy_external_machine_type = "n1-highcpu-2"

  monitor_node_count = 1
  monitor_machine_type = "n1-highcpu-2"

  postgres_node_count = 1
  postgres_machine_type = "n1-standard-2"

  redis_node_count = 1
  redis_machine_type = "n1-standard-1"
}

output "gitlab_ref_arch_gcp" {
  value = module.gitlab_ref_arch_gcp
}
