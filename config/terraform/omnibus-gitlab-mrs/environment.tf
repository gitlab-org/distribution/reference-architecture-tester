module "gitlab_ref_arch_gcp" {
  source = "../modules/gitlab_ref_arch_gcp"

  prefix = var.prefix
  project = var.project
  service_account_prefix = var.sa_prefix

  machine_image = "ubuntu-2204-lts"

  consul_node_count = 3
  consul_machine_type = "g1-small"

  postgres_node_count = 3
  postgres_machine_type = "n1-standard-1"

  gitlab_rails_node_count = 1
  gitlab_rails_machine_type = "n1-standard-16"

  pgbouncer_node_count = 1
  pgbouncer_machine_type = "g1-small"

  # GET must-haves
  haproxy_external_node_count = 1
  haproxy_external_machine_type = "g1-small"
  haproxy_internal_node_count = 1
  haproxy_internal_machine_type = "g1-small"
}

output "gitlab_ref_arch_gcp" {
  value = module.gitlab_ref_arch_gcp
}
