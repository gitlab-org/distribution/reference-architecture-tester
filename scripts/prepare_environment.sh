#!/bin/bash

shopt -s extglob

if [ $# -eq 0 ]; then
  echo "Usage: prepare_environment <terraform|ansible>"
  exit 1
fi

function prepare_ansible() {
  prepare_private_key
  prepare_license_file
  prepare_ansible_directories
  prepare_infrastructure_details
  prepare_package_details
  prepare_gitlab_details
}

function prepare_private_key() {
  if ! [ -e "${SSH_PRIVATE_KEY_FILE}" ]; then
    echo "${SSH_PRIVATE_KEY}" > "${SSH_PRIVATE_KEY_FILE}" && chmod 0600 "${SSH_PRIVATE_KEY_FILE}"
  fi
}

function prepare_license_file() {
  if ! [ -e "${GITLAB_LICENSE_FILE}" ]; then
    echo "${GITLAB_LICENSE}" > "${GITLAB_LICENSE_FILE}"
  fi
}

function prepare_ansible_directories() {
  mkdir -p gitlab-environment-toolkit/ansible/environments/rat
  cp -vr config/ansible/inventory gitlab-environment-toolkit/ansible/environments/rat/inventory

  if [ -d config/ansible/extra_files/${REFERENCE_ARCHITECTURE} ]; then
    cp -vr config/ansible/extra_files/${REFERENCE_ARCHITECTURE}/* gitlab-environment-toolkit/ansible/environments/rat
  fi
}

function prepare_infrastructure_details() {
  sed -i "s|<RAT_GCP_PREFIX>|${TF_VAR_prefix}|g;
          s|<RAT_GCP_PROJECT>|${TF_VAR_project}|g;
          s|<RAT_ANSIBLE_USER>|${ANSIBLE_USER}|g;
          s|<RAT_EXTERNAL_IP>|${EXTERNAL_IP}|g;
          s|<RAT_GITLAB_INITIAL_PASSWORD>|${GITLAB_INITIAL_PASSWORD}|g;
          s|<RAT_SSH_PRIVATE_KEY_FILE>|${SSH_PRIVATE_KEY_FILE}|g;
          s|<RAT_GITLAB_LICENSE_FILE>|${GITLAB_LICENSE_FILE}|g;
          s|<RAT_GCP_SERVICE_ACCOUNT_HOST_FILE>|${GITLAB_GCP_SERVICE_ACCOUNT_FILE:-${GCP_SERVICE_ACCOUNT_FILE}}|g;" gitlab-environment-toolkit/ansible/environments/rat/inventory/*.yml
}

function prepare_gitlab_details() {
  if [ "$NO_GITLAB_LICENSE" == "true" ]; then
    sed -i "/gitlab_license_file/d" gitlab-environment-toolkit/ansible/environments/rat/inventory/vars.yml
  fi
}

function prepare_package_details() {
  if [ -n "${PACKAGE_URL}" ]; then
    prepare_package_url
  else
    prepare_package_repository
    prepare_package_version
  fi
}

function prepare_package_url() {
  echo "Configuring GET to fetch package from ${PACKAGE_URL}"

  if [[ ${PACKAGE_URL} == *rpm ]]; then
    extension=rpm
  else
    extension=deb
  fi

  if [[ ${PACKAGE_URL} == https://gitlab.com/api/v4* ]]; then
    sed -i "s|# gitlab_${extension}_download_url_headers|gitlab_${extension}_download_url_headers|g" gitlab-environment-toolkit/ansible/environments/rat/inventory/vars.yml
  fi

  sed -i "s|# gitlab_${extension}_download_url|gitlab_${extension}_download_url|g" gitlab-environment-toolkit/ansible/environments/rat/inventory/vars.yml

}

function prepare_nightly_repository() {
  if [[ "${REFERENCE_ARCHITECTURE}" =~ "omnibus-gitlab-mrs-fips" ]]; then
    echo "Configuring GET to fetch package from FIPS nightly repository"
    sed -i "s|# gitlab_repo_script_url.*|gitlab_repo_script_url: \"https://packages.gitlab.com/install/repositories/gitlab/nightly-fips-builds/script.rpm.sh\"|g" gitlab-environment-toolkit/ansible/environments/rat/inventory/vars.yml
    sed -i "s|# postgres_host.*|postgres_host: \"{{ lookup('env','CLOUD_SQL_HOST') }}\"|g" gitlab-environment-toolkit/ansible/environments/rat/inventory/vars.yml
  else
    echo "Configuring GET to fetch package from nightly repository"
    sed -i "s|# gitlab_repo_script_url.*|gitlab_repo_script_url: \"https://packages.gitlab.com/install/repositories/gitlab/nightly-builds/script.deb.sh\"|g" gitlab-environment-toolkit/ansible/environments/rat/inventory/vars.yml
  fi
}

function prepare_pre_release_repository() {
  echo "Configuring GET to fetch package from pre-release repository"
  sed -i "s|# gitlab_repo_script_url.*|gitlab_repo_script_url: \"${GITLAB_PRE_RELEASE_REPO_SCRIPT}\"|g" gitlab-environment-toolkit/ansible/environments/rat/inventory/vars.yml
}

function prepare_package_repository() {
  if [ "$NIGHTLY" == "true" ]; then
    prepare_nightly_repository
  elif [ "$PRE_RELEASE" == "true" ]; then
    prepare_pre_release_repository
  fi
}

function prepare_package_version() {
  if [ -n "${PACKAGE_VERSION}" ]; then
    echo "Configuring GET to install package ${PACKAGE_VERSION}"
    sed -i "s|# gitlab_repo_package.*|gitlab_repo_package: \"${PACKAGE_VERSION}\"|g" gitlab-environment-toolkit/ansible/environments/rat/inventory/vars.yml
  fi
}

if ! [ -d secrets ]; then mkdir secrets; fi

if [ -n "${GCP_SERVICE_ACCOUNT_JSON}" ] && ! [ -e "${GCP_SERVICE_ACCOUNT_FILE}" ]; then
  echo "${GCP_SERVICE_ACCOUNT_JSON}" > "${GCP_SERVICE_ACCOUNT_FILE}"
fi

if [ "$1" == "terraform" ]; then
  cp -vr config/terraform/${REFERENCE_ARCHITECTURE}/* gitlab-environment-toolkit/terraform/environments
elif [ "$1" == "ansible" ]; then
  prepare_ansible
fi
